#!/bin/bash

#===========================
#Set the apt command
#===========================
sudo apt-get -y update
sudo apt-get -y upgrade

#===========================
#Set Japanese 
#===========================
sudo locale-gen ja_JP.UTF-8
sudo locale-gen ja_JP.EUC-JP
sudo apt-get -y install fonts-vlgothic
#sudo apt-get -y install ibus-anthy
sudo apt-get -y install fcitx-mozc
sudo localectl set-keymap jp106
sudo echo alias startx=\'LANG=ja_JP.UTF-8 startx\' >> .bashrc

#===========================
#Set TimeZone
#===========================
echo "Asia/Tokyo" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata

#===========================
#Get Midori
#===========================
#sudo apt-get -y install midori

#===========================
#Install Printer
#===========================
sudo apt-get -y install cups
sudo apt-get -y install system-config-printer
sudo apt-get -y install printer-driver-escpr

#===========================
#Install Geany
#===========================
sudo apt-get -y install geany

#===========================
#Install pip easy-install
#===========================
sudo apt-get -y install python-pip
sudo pip install --upgrade pip
sudo rm /usr/bin/pip
sudo pip install requests_oauthlib
sudo pip install requests

#===========================
# Install CRON-GUI
#===========================
sudo apt-get -y install gnome-schedule

#===========================
#Install LaTeX
#===========================
#sudo apt-get -y install texlive-lang-cjk
#tlmgr init-usertree

#===========================
#Install Printer
#===========================
#sudo apt-get -y install printer-driver-escpr

#===========================
#Install Fortran
#===========================
sudo apt-get -y install gfortran

#===========================
#The END
#===========================
echo 'finished.'
echo "Do you want to reboot NOW? [Y/n]"
read ANSWER
case $ANSWER in
    "" | "Y" | "y" | "yes" | "Yes" | "YES" ) sudo reboot ;;
    * ) echo "You have to reboot later.";;
esac

