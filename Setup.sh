#!/bin/bash
#===========================
# Easy setting for Ubuntu 14.04LTS 
# 2015/10/24
# Author : kamome_Mew
#===========================

VER=`grep CODENAME /etc/lsb-release | sed "s/DISTRIB_CODENAME=//g"`

#===========================
# Update&Upgrade
#===========================
sudo apt-get -y update 
sudo apt-get -y upgrade 

#===========================
# LXDE-Desktop-environment
#===========================
#sudo apt-get install lxde
#sudo apt-get install lubuntu-desktop

#===========================
# MATE-Desktop-environments
# Version dependentapt-add-repository -y ppa:ubuntu-mate-dev/$VER-mate
#===========================
sudo apt-add-repository -y ppa:ubuntu-mate-dev/ppa 
sudo apt-add-repository -y ppa:ubuntu-mate-dev/$VER-mate 
sudo apt-get -y update 
sudo apt-get -y upgrade 
sudo apt-get -y install mate-desktop-environment-core 
sudo apt-get -y install mate-desktop-environment 
sudo apt-get -y install mate-desktop-environment-extras 

#===========================
# Rename Japanese Files
#===========================
env LANGUAGE=C LC_MESSAGES=C xdg-user-dirs-gtk-update

#===========================
# Remove other browsers
#===========================
sudo apt-get -y --purge remove firefox
sudo apt-get -y --purge remove webbrowser-app

#===========================
# Remove Web Apps
#===========================
sudo apt-get -y remove unity-lens-video
sudo apt-get -y remove unity-scope-video-remote 
sudo apt-get -y remove unity-lens-shopping 
sudo apt-get -y remove unity-lens-music
sudo apt-get -y remove unity-lens-photos
sudo apt-get -y remove unity-scope-gdrive

#===========================
# Install Chrome
#===========================
#sudo apt-get -y install chromium-browser chromium-browser-l10n
#sudo apt-get -y install pepperflashplugin-nonfree
#sudo update-pepperflashplugin-nonfree --install

#Chrome => 
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get -y update
sudo apt-get -y install google-chrome-stable
sudo rm /etc/apt/sources.list.d/google.list

#===========================
# Install Editor
#===========================
sudo apt-get -y install geany
#sudo apt-get -y install emacs
#sudo apt-get -y install vim
#sudo apt-get -y install gedit-plugins

#sudo add-apt-repository ppa:webupd8team/atom
#sudo apt-get update
#sudo apt-get install atom

#===========================
# Install pip easy-install
#===========================
sudo apt-get -y install python-pip
sudo pip install requests_oauthlib

#===========================
# Install CRON-GUI
#===========================
sudo apt-get -y install gnome-schedule

#===========================
# Install LaTeX
#===========================
sudo apt-get -y install texlive-full
tlmgr init-usertree
sudo apt-get -y install texstudio 

#===========================
# WiFi Re-configure for MATE
#===========================
#managed=false => managed=true;
#sudo sed -i -e 's/managed=false/managed=true;/g'
#GNOME3only => None
sudo sed  -i -e 's/Autostart/#Autostart/g' /etc/xdg/autostart/nm-applet.desktop

#===========================
# The Others
#===========================
gsettings set org.mate.caja.desktop computer-icon-visible false
sudo apt-get -y remove pluma
sudo apt-get -y remove eom

#===========================
# If you use VAIO...
#===========================
#sudo sed  -i -e 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="acpi_sleep=nonvs"/g' /etc/default/grub

#!! If you don't do this, you cannot suspend !!

#===========================
# Printer
#===========================
#sudo apt-get -y install epson-inkjet-printer-escpr
#sudo apt-get -y install printer-driver-escpr
#sudo apt-get -y install cnijfilter-ipxxxxseries
# xxxx is your model number.

#===========================
# Git & Git-gui
#===========================
#sudo apt-get -y install git
#sudo apt-get -y install git-gui
#sudo apt-get -y install gitk


#===========================
# The END
#===========================
sudo apt-get -y autoclean 
sudo apt-get -y autoremove
sudo update-grub
echo -e 'finished.'
echo -e "Do you want to reboot NOW? [Y/n]"
read ANSWER
case $ANSWER in
    "" | "Y" | "y" | "yes" | "Yes" | "YES" ) sudo reboot ;;
    * ) echo "You have to reboot later.";;
esac
