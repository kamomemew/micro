#coding:utf-8

import os
import sys
import markdown
import codecs


sty="""<style type="text/css">
body{background-color:#cfc;color:#111;font-size:10pt;margin-left:25px;}
a:link { color: #00f; }
a:visited { color: #008; }
a:hover { color: #f00; }
a:active { color: #f80; }
h1{font-size:200%;line-height: 100%}
h2{font-size:170%;line-height: 100%}
h3{font-size:140%;line-height: 100%}
table{
border:1px solid #004400;text-align:center;
}
td,th{border:solid 1px #004400;padding: 2pt;}
th{font-weight:bold;}
.ttle{text-align:center;}
#rireki{height:120px;overflow:scroll;display:block;}
table#menu1{
border:1px solid #004400;text-align:center;
position:absolute; 
left:10%; 
width:80%; 
}
td{border:solid 1px #004400;padding: 2pt;}
@media screen and (min-width: 721px){
td{width:33%;}
td.ketsugou{width:100%;}
}
@media screen and (max-width: 720px){
#menu1 tr {
border:none;
}
#menu1 td {
border-bottom: none;
display: block;
padding: 2pt;
	
}
#menu1 .lst {
border-bottom: 1px solid #004400;
}
@media screen and (max-width: 530px){
img#topga{    width: 100%;
height: auto;}
}
</style>

""".decode('utf-8')

meta_tag="""<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
"""

doc_type='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">'


def md2html_mode (path_to_md):
	if path_to_md[-2:] != "md":
		print("拡張子は~.mdです。\n markdownではないので終了します")
		quit()
	md = markdown.Markdown()
	f_m = open(path_to_md,"r")
	d=os.path.dirname(path_to_md)
	md_text=f_m.read().decode('utf-8')
	htm=md.convert(md_text)
	lines=htm.split("\n")
	print("タイトルの自動推測をしますか(Y/N)")
	ans_title=raw_input()
	if ans_title == "Y":
		title=""	
		for line in lines:
			if "<h1>" in line:
				title=line.replace("<h1>","")
				title=title.replace("</h1>","")	
				title=title.replace(" ","")	
				title="<title>"+title+"</title>"
				break
		if title == "":
			print("タイトルが見つかりませんでした。入力してください。")
			title=raw_input()
			title="<title>"+title+"</title>"
	else :
		print("タイトルを入力")
		title=raw_input()
		title="<title>"+title+"</title>"
	
	htm=htm.replace(" />",">")
	
	htm=u"<body>\n"+htm+u"\n</body>"
	htm=u"<head>\n"+meta_tag+sty+title+u"\n</head>"+htm
	htm=u'\n<html>\n'+htm+u"\n</html>"
	htm=doc_type+htm
	
	path_to_htm=path_to_md[:-2]+u"html"
	f = codecs.open(path_to_htm,"w","utf-8")
	f.write(htm)
	f.close()
	quit()
	

def check_command (p):
	if os.path.isfile(p):
		print("Convert md to html.")
		md2html_mode(p)
	else:
		exit
	

if __name__ == '__main__':
	f_name=sys.argv[1]
	f_name=f_name.replace('"',"")
	f_name=f_name.replace("'","")
	check_command(f_name)
