@powershell -NoProfile -ExecutionPolicy Unrestricted "$s=[scriptblock]::create((gc \"%~f0\"|?{$_.readcount -gt 1})-join\"`n\");&$s" %*&goto:eof

##################################################################
# 東北大学住井・松田研究室様が公開している
# https://github.com/fetburner/sumiilab-tex/blob/master/install.sh
# を、Windows上で動作するようにしたものです。
# PowerShell4.0以上でないと動きません。
# バッチファイルを管理者として実行してください。
##################################################################


function check_PS_version {
    if ($HOST.Version.Major -lt 4){
        Write-Host 'Error!' -ForeGroundColor Red
        $wsobj  = new-object -comobject wscript.shell
        $result = $wsobj.popup("PowerShellが4.0未満です。終了します")
        exit 1
    }
    else {
        Write-Host 'PowerShell -Version'$HOST.Version.Major -ForeGroundColor Green
    
    }
}

function check_command ($CMD){
    Write-Host $CMD"コマンド:" -NoNewline
    if (gcm $CMD -ea SilentlyContinue) {
        Write-Host 'OK' -ForeGroundColor Green
    } 
    else {
        Write-Host 'Error!' -ForeGroundColor Red
        $wsobj  = new-object -comobject wscript.shell
        $result = $wsobj.popup($CMD+"コマンドが見つからないので終了します")
        exit 1
    }
}

function check_sty($STY){
    Write-Host $STY"スタイル:" -NoNewline
    $EXIST=kpsewhich $STY
    if($EXIST){
        Write-Host 'インストール済' -ForeGroundColor Green
        return 0
    }
    else{
        Write-Host '未インストール' -ForeGroundColor Yellow
        return 1
    }
}

function Expand-ZIPFile($file, $destination)
{
    $shell = new-object -com shell.application
    $zip = $shell.NameSpace($file)
    foreach($item in $zip.items()){
        $shell.Namespace($destination).copyhere($item)
    }
}

check_PS_version
check_command wget
check_command cp
check_command ni
check_command kpsewhich
check_command mktexlsr


Write-Host "TeXディレクトリ:" -NoNewline
$TEXDIR=kpsewhich amsmath.sty
$TEXDIR=$TEXDIR -split "/tex/"
$TEXDIR=$TEXDIR[0]+"/tex"

if ($TEXDIR){
    Write-Host '検出' -ForeGroundColor Green
}
else{
    Write-Host 'Error!' -ForeGroundColor Red
    $wsobj  = new-object -comobject wscript.shell
    $result = $wsobj.popup("TeXディレクトリが見つからないので終了します")
    exit 1
}

mkdir ./WKDR_TEX
cd ./WKDR_TEX
$WKDR=Resolve-Path ./

if(check_sty bcprules.sty){
    wget http://www.cis.upenn.edu/~bcpierce/papers/bcprules.sty -OutFile bcprules.sty
    New-Item $TEXDIR/latex/bcprules -itemType Directory
    Copy-Item $WKDR/bcprules.sty $TEXDIR/latex/bcprules/bcprules.sty
}

if(check_sty proof.sty){
    wget http://research.nii.ac.jp/~tatsuta/proof.sty -OutFile proof.sty
    New-Item $TEXDIR/latex/proof -itemType Directory
    Copy-Item $WKDR/proof.sty $TEXDIR/latex/proof/proof.sty
}

if(check_sty bussproofs.sty){
    wget http://math.ucsd.edu/~sbuss/ResearchWeb/bussproofs/bussproofs.sty -OutFile bussproofs.sty
    New-Item $TEXDIR/latex/bussproofs -itemType Directory
    Copy-Item $WKDR/bussproofs.sty $TEXDIR/latex/bussproofs/bussproofs.sty
}

if(check_sty jlisting.sty){
    wget http://captain.kanpaku.jp/LaTeX/jlisting.zip -OutFile jlisting.zip
    Expand-ZIPFile jlisting.zip $WKDR
    New-Item $TEXDIR/platex/jlisting -itemType Directory
    Copy-Item $WKDR/jlisting/jlisting.sty $TEXDIR/platex/jlisting/jlisting.sty
}

if(check_sty pxjahyper.sty){
    wget https://github.com/zr-tex8r/PXjahyper/archive/master.zip -OutFile PXjahyper.zip
    Expand-ZIPFile PXjahyper.zip $WKDR
    New-Item $TEXDIR/platex/pxjahyper -itemType Directory
    Copy-Item $WKDR/PXjahyper-master/pxjahyper.sty $TEXDIR/platex/pxjahyper/pxjahyper.sty
}

if(check_sty algorithm.sty){
    wget http://mirrors.ctan.org/macros/latex/contrib/algorithms.zip -OutFile algorithms.zip
    Expand-ZIPFile algorithms.zip $WKDR
    New-Item $TEXDIR/latex/algorithms -itemType Directory
    Copy-Item $WKDR/algorithms/algorithms.sty $TEXDIR/latex/algorithms/algorithm.sty
}

mktexlsr
cd ../
Remove-Item ./WKDR_TEX -Recurse
